use std::rc::Rc;

use handlebars::{
    handlebars_helper, Context, Handlebars, Helper, HelperDef, JsonValue, PathAndJson,
    RenderContext, RenderError, RenderErrorReason, ScopedJson,
};
use regex::Regex;

use crate::template::{ConcatHelper, DefaultHelper, RegexReplaceHelper, SetHelper, TimeHelper};

handlebars_helper!(replace: |input: str, from: str, to: str| {
    input.replace(from, to)
});

/// Adds the template helpers to the given handlebars instance
pub fn add_helpers(bars: &mut Handlebars) {
    bars.register_helper("regex_replace", Box::new(RegexReplaceHelper));
    bars.register_helper("concat", Box::new(ConcatHelper));
    bars.register_helper("default", Box::new(DefaultHelper));
    bars.register_helper("set", Box::new(SetHelper));
    bars.register_helper("replace", Box::new(replace));
    bars.register_helper("time", Box::new(TimeHelper));
}

impl HelperDef for RegexReplaceHelper {
    fn call_inner<'reg: 'rc, 'rc>(
        &self,
        helper: &Helper<'rc>,
        _: &'reg Handlebars<'reg>,
        _: &'rc Context,
        _: &mut RenderContext<'reg, 'rc>,
    ) -> Result<ScopedJson<'rc>, RenderError> {
        let params = helper.params();

        if params.len() != 3 {
            return Err(RenderErrorReason::Other(
                "Invalid replace arguments. Usage: {{replace <input> <from> <to>}}".to_string(),
            )
            .into());
        }

        if let [input, from, to] = &params[..] {
            let input = input.value().as_str().ok_or(RenderErrorReason::Other(
                "Argument input has to be a string".to_string(),
            ))?;
            let from = from.value().as_str().ok_or(RenderErrorReason::Other(
                "Argument from has to be a string".to_string(),
            ))?;
            let to = to.value().as_str().ok_or(RenderErrorReason::Other(
                "Argument to has to be a string".to_string(),
            ))?;

            let result = Regex::new(from).unwrap().replace_all(input, to).to_string();

            return Ok(ScopedJson::Derived(JsonValue::String(result)));
        }

        Err(RenderErrorReason::Other("Could not replace. Unknown error.".to_string()).into())
    }
}

impl ConcatHelper {
    fn stringify_params(&self, params: &Vec<PathAndJson>) -> Vec<String> {
        let mut strings = vec![];

        for value in params {
            strings.push(value.render());
        }

        strings
    }
}

impl HelperDef for ConcatHelper {
    fn call_inner<'reg: 'rc, 'rc>(
        &self,
        helper: &Helper<'rc>,
        _: &'reg Handlebars<'reg>,
        _: &'rc Context,
        _: &mut RenderContext<'reg, 'rc>,
    ) -> Result<ScopedJson<'rc>, RenderError> {
        let params = helper.params();
        let strings = self.stringify_params(params);

        Ok(ScopedJson::Derived(JsonValue::String(
            strings.join(&String::new()),
        )))
    }
}

impl HelperDef for DefaultHelper {
    fn call_inner<'reg: 'rc, 'rc>(
        &self,
        helper: &Helper<'rc>,
        _: &'reg Handlebars<'reg>,
        _: &'rc Context,
        _: &mut RenderContext<'reg, 'rc>,
    ) -> Result<ScopedJson<'rc>, RenderError> {
        let arguments = helper.params();
        let value = arguments.get(0).ok_or(RenderErrorReason::Other(
            "Missing value argument".to_string(),
        ))?;
        let fallback = arguments.get(1).ok_or(RenderErrorReason::Other(
            "Missing fallback argument".to_string(),
        ))?;

        if value.is_value_missing() {
            return Ok(ScopedJson::Derived(fallback.value().clone()));
        }

        let value = value.value().clone();

        if value == 0 || value == "" || value == String::new() || value == false || value.is_null()
        {
            return Ok(ScopedJson::Derived(fallback.value().clone()));
        }

        Ok(ScopedJson::Derived(value))
    }
}

impl HelperDef for SetHelper {
    fn call_inner<'reg: 'rc, 'rc>(
        &self,
        helper: &Helper<'rc>,
        _: &'reg Handlebars<'reg>,
        default_context: &'rc Context,
        render_context: &mut RenderContext<'reg, 'rc>,
    ) -> Result<ScopedJson<'rc>, RenderError> {
        let arguments = helper.params();
        let variable_name = arguments.get(0).ok_or(RenderErrorReason::Other(
            "Missing variable_name argument".to_string(),
        ))?;
        let content = arguments.get(1).ok_or(RenderErrorReason::Other(
            "Missing content argument".to_string(),
        ))?;

        let variable_name = variable_name.render();
        let content = content.value();

        let mut ctx = render_context
            .context()
            .unwrap_or(Rc::new(default_context.clone()))
            .as_ref()
            .clone();
        let data = ctx.data_mut();

        data[variable_name] = content.clone();

        render_context.set_context(ctx);

        Ok(ScopedJson::Missing)
    }
}

impl HelperDef for TimeHelper {
    fn call_inner<'reg: 'rc, 'rc>(
        &self,
        helper: &Helper<'rc>,
        _: &'reg Handlebars<'reg>,
        _: &'rc Context,
        _: &mut RenderContext<'reg, 'rc>,
    ) -> Result<ScopedJson<'rc>, RenderError> {
        let arguments = helper.params();
        let default_value = String::from("%c");
        let default_path = PathAndJson::new(
            None,
            ScopedJson::Derived(JsonValue::String(default_value.clone())),
        );
        let date_format = arguments.get(0).unwrap_or(&default_path);
        let date_format = date_format
            .value()
            .as_str()
            .unwrap_or(default_value.as_str());

        let output = chrono::Local::now().format(date_format).to_string();

        Ok(ScopedJson::Derived(JsonValue::String(output)))
    }
}
