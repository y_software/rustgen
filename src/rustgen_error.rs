use std::error::Error;
use std::fmt::{Debug, Display, Formatter};
use std::{fmt, io};

use handlebars::RenderError;

pub type RustgenResult<T> = Result<T, RustgenError>;

pub struct RustgenError {
    message: String,
    debug: String,
    context: Option<String>,
}

impl RustgenError {
    pub fn new(message: impl ToString) -> Self {
        Self {
            debug: message.to_string(),
            message: message.to_string(),
            context: None,
        }
    }

    pub fn context(mut self, context: impl ToString) -> Self {
        self.context = Some(context.to_string());

        self
    }

    pub fn get_context(&self) -> Option<&String> {
        self.context.as_ref()
    }
}

impl Error for RustgenError {}

impl Display for RustgenError {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        formatter.write_str(&self.message)
    }
}

impl Debug for RustgenError {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        formatter.write_str(&self.debug)
    }
}

impl From<serde_yml::Error> for RustgenError {
    fn from(parent: serde_yml::Error) -> Self {
        Self {
            message: format!("Could not (de-)serialize: {}", parent),
            debug: format!("{:?}", parent),
            context: None,
        }
    }
}

impl From<io::Error> for RustgenError {
    fn from(parent: io::Error) -> Self {
        Self {
            message: format!(
                "Could not fulfill file operation: {}\n Error: {:?}",
                parent,
                parent.kind()
            ),
            debug: format!("{:?}", parent),
            context: None,
        }
    }
}

impl From<RenderError> for RustgenError {
    fn from(parent: RenderError) -> Self {
        Self {
            message: format!("Could not render template: {}", parent),
            debug: format!("{:?}", parent),
            context: None,
        }
    }
}
