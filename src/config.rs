use std::collections::HashMap;
use std::env::current_dir;
use std::fs;
use std::fs::{OpenOptions, ReadDir};

use std::path::{Path, PathBuf};

use serde::{Deserialize, Serialize};

use crate::rustgen_error::RustgenError;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum TemplatePath {
    One(String),
    Many(Vec<String>),
}

/// Available application config
///
/// # Options
///
/// | Name | Description | Default |
/// | --- | --- | --- |
/// | `template_path` | Path to the templates root directory. Containing structure has to match "_generator/TYPE/ACTION/template.hbs" | `_generator` |
/// | `defaults` | Default variables set in templates (e.g. the name of your *main* plugin). | *None* |
///
/// # Example
///
/// ```yaml
/// template_path: ".generator"
/// defaults:
///     basepath: "./plugin/MyPlugin"
/// ```
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ApplicationConfig {
    #[serde(default)]
    pub template_path: TemplatePath,
    #[serde(default)]
    pub default: HashMap<String, String>,
}

#[derive(Clone, Debug)]
pub struct ResolvedTemplate {
    pub path: PathBuf,
}

#[derive(Clone, Debug)]
pub struct ResolvedTemplates {
    templates: HashMap<String, HashMap<String, Vec<ResolvedTemplate>>>,
}

/// Reads the Application config from the paths (in the following order)
/// - ./.rustgenrc.yaml
/// - ./.rustgenrc.yml
///
pub fn read() -> ApplicationConfig {
    let mut config = ApplicationConfig::default();

    let global_config = std::env::var("RUSTGEN_GLOBAL_CONFIG")
        .unwrap_or_else(|_| (std::env::var("HOME").unwrap_or_default() + "/.rustgenrc.yaml"));

    if let Ok(loaded_config) = load_config_file(global_config) {
        config = config.merge(loaded_config);
    }

    if let Ok(loaded_config) = load_config_file(
        current_dir()
            .unwrap_or(PathBuf::new())
            .join(".rustgenrc.yaml"),
    ) {
        config = config.merge(loaded_config);
    }

    if let Ok(loaded_config) = load_config_file(
        current_dir()
            .unwrap_or(PathBuf::new())
            .join(".rustgenrc.yml"),
    ) {
        config = config.merge(loaded_config);
    }

    config
}

impl TemplatePath {
    pub fn round_up(&self) -> Vec<String> {
        match self.clone() {
            TemplatePath::One(path) => vec![path],
            TemplatePath::Many(paths) => paths,
        }
    }
}

impl Default for TemplatePath {
    fn default() -> Self {
        TemplatePath::One("_generator".to_string())
    }
}

impl ResolvedTemplates {
    pub fn new() -> Self {
        Self {
            templates: Default::default(),
        }
    }

    pub fn load_directory<P: AsRef<Path>>(&mut self, path: P) -> Result<(), RustgenError> {
        let path = PathBuf::new().join(path);

        if path.is_file() {
            return Err(RustgenError::new(format!("Template path is a file")));
        }

        self.load_types(path)
    }

    pub fn get_template_files(
        self,
        typ: &String,
        action: &String,
    ) -> Option<Vec<ResolvedTemplate>> {
        self.templates
            .get(typ)
            .and_then(|value| value.get(action).map(Clone::clone))
    }

    pub fn iter(&self) -> HashMap<String, Vec<String>> {
        let mut map = HashMap::default();

        for (typ, actions) in self.templates.iter() {
            map.insert(
                typ.clone(),
                actions.keys().map(|name| name.clone()).collect(),
            );
        }

        map
    }

    fn load_types(&mut self, path: PathBuf) -> Result<(), RustgenError> {
        let dir = fs::read_dir(path).map_err(|_| RustgenError::new("Could not load types"))?;

        for (name, path) in Self::get_directory_entries(dir) {
            if path.is_file() {
                continue;
            }

            if !self.templates.contains_key(&name) {
                self.templates.insert(name.clone(), Default::default());
            }

            self.load_actions(name, path);
        }

        Ok(())
    }

    fn get_directory_entries(dir: ReadDir) -> Vec<(String, PathBuf)> {
        let mut entries = vec![];

        for entry in dir {
            let entry = if let Ok(entry) = entry {
                entry
            } else {
                continue;
            };

            let name = entry.file_name().to_str().unwrap().to_string();

            entries.push((name, entry.path()));
        }

        entries
    }

    fn load_actions(&mut self, typ: String, path: PathBuf) {
        let dir = if let Ok(dir) = fs::read_dir(path) {
            dir
        } else {
            return;
        };

        for (name, path) in Self::get_directory_entries(dir) {
            if path.is_file() {
                continue;
            }

            if let Some(actions) = self.templates.get_mut(&typ) {
                actions.insert(name.clone(), Default::default());
            }

            self.load_templates(typ.clone(), name, path);
        }
    }

    fn load_templates(&mut self, typ: String, action: String, path: PathBuf) {
        let dir = if let Ok(dir) = fs::read_dir(path) {
            dir
        } else {
            return;
        };
        let mut resolved_templates = vec![];

        for (_, path) in Self::get_directory_entries(dir) {
            if path.is_dir() {
                continue;
            }

            resolved_templates.push(ResolvedTemplate { path });
        }

        if let Some(templates) = self.templates.get_mut(&typ) {
            templates.insert(action, resolved_templates);
        }
    }
}

fn load_config_file<P: AsRef<Path>>(path: P) -> Result<ApplicationConfig, RustgenError> {
    let path = PathBuf::from(path.as_ref());

    if !path.exists() {
        return Err(RustgenError::new("Path does not exist"));
    }

    if path.is_dir() {
        eprintln!("Config {:?} is a directory", path);

        return Err(RustgenError::new("Path is a directory"));
    }

    let file = OpenOptions::new()
        .read(true)
        .write(false)
        .create(false)
        .open(path)?;

    Ok(serde_yml::from_reader(file)?)
}

impl ApplicationConfig {
    ///
    /// Merges config fields
    ///
    /// Template paths that exist in the current instance will be moved to the end
    ///
    pub fn merge(mut self, other: Self) -> Self {
        self.template_path = TemplatePath::Many(self.template_path.round_up());

        if let TemplatePath::Many(template_paths) = &mut self.template_path {
            let other_paths = other.template_path.round_up();
            *template_paths = template_paths
                .iter()
                .filter(|value| !other_paths.contains(value))
                .map(Clone::clone)
                .collect();

            template_paths.extend(other_paths);
        }

        self.default.extend(other.default);

        self
    }

    ///
    /// Gets all available templates from all configured directories
    ///
    pub fn get_templates(&self) -> Result<ResolvedTemplates, RustgenError> {
        let mut templates = ResolvedTemplates::new();

        for template_path in self.template_path.round_up() {
            if let Err(error) = templates.load_directory(&template_path) {
                eprintln!("Could not load path {}: {}", template_path, error);
            }
        }

        Ok(templates)
    }
}

impl Default for ApplicationConfig {
    fn default() -> Self {
        ApplicationConfig {
            template_path: Default::default(),
            default: Default::default(),
        }
    }
}
